function settings() {
    return {
        headColor: "#f9af2b",
        //colors: ["#C1272D", "#ED1C24", "#F15A24", "#F7931E", "#FBB03B", "#FCEE21", "#D9E021", "#8CC63F", "#39B54A", "#009245", "#22B573", "#00A99D", "#29ABE2", "#0071BC", "#2E3192", "#1B1464", "#662D91", "#93278F", "#9E005D", "#D4145A"],
        colors: ["#10a0f9"],
        title: "test", // der Titel, der im Tab steht
        fps: 8, // frames per seccond, mehr = schneller, weniger = langsamer
        showScore: true, // soll die Punktezahl angezeigt werden?
        size: 20, // die Grösse des Spieles in Pixel
        keyboardShortcuts: {
            top: "w",
            left: "a",
            right: "d",
            bottom: "s"
        },
        fieldSize: [20, 20], // ist false, wenn unendlich gross oder ein Array beispiel: [20,20] ist also 20 breit und 20 hoch
        activeKey: " ", // Taste, die gedrückt werden muss, damit das Spiel angezeigt wird (" " bedeutet Leertaste)
        scoreKey: "x", // Taste, die gedrückt werden muss, damit der Score angezeigt wird
        version: 1.0
    };
}

/*
Vorlage, wenn man mit Pfeiltasten spielen will.
(einfach bereits existierende KeyboardShortcuts ersetzen)

keyboardShortcuts: {
    top: "ArrowUp",
    left: "ArrowLeft",
    right: "ArrowRight",
    bottom: "ArrowDown"
}
*/